# Project Overview

Test project Artem Redchych

## Features

### Splash Screen
- The application greets users with a splash screen upon launch. 
- **Note:** There is a need for a better(fixed size), more appealing icon to improve the first impression.

### Welcome Screen
- Following the splash screen, users are presented with a welcome screen. This screen serves as an introduction to the application, guiding users on what to expect as they proceed.

### Sign-In Screen


### My Collection Screen
- This screen fetches and displays data from a mock file, showcasing the user's collection of items.
- **Internet Connectivity Check:** The application checks for an internet connection when loading products. If the internet is disabled, it notifies the user and suggests turning the internet on and retrying.

### Product Details Screen
- Users can view detailed information about each product in their collection through the Product Details screen.
- **Remove from Collection:** Users have the option to remove items from their collection directly from this screen.
- **Offline Handling:** If the internet connection is disabled during the removal process, the application temporarily stores the data in memory and updates the collection once a successful connection is re-established.

## Architecture

### Blocs
The application leverages the Bloc pattern for state management, utilizing two main Blocs:

1. **Products Bloc**
   - Manages events and states related to product operations (e.g., fetching products, removing products from the collection).
   - **Connectivity Handling:** Instead of embedding an Internet Bloc within the Products Bloc, the application uses a `ConnectivityRepository` within the Products Bloc. This design choice allows for a more modular and flexible approach to handling internet connectivity issues.

2. **Internet Connection Bloc**
   - Currently not used directly within the application. The initial design was to have this Bloc manage internet connectivity states and events. However, the decision to use `ConnectivityRepository` provides a streamlined way to handle connectivity within the Products Bloc itself.

## Development Time Breakdown

### Welcome Screen
- **Time Spent:** 60 minutes

### Sign-In Screen
- **Time Spent:** 30 minutes

### My Collection Screen + Bloc + Product Detail Screen
- **Time Spent:** 5.5 hours

### Different solutions for internet connection

- **Time Spent:** 2.5 hours
