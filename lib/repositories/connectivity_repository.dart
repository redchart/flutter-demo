import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectivityRepository {
  final Connectivity _connectivity = Connectivity();
  final StreamController<bool> _controller = StreamController<bool>();

  ConnectivityRepository() {
    _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _controller.add(_isConnected(result));
    });
  }

  Stream<bool> get connectivityStream => _controller.stream;

  Future<bool> isConnected() async {
    var result = await _connectivity.checkConnectivity();
    return _isConnected(result);
  }

  bool _isConnected(ConnectivityResult result) {
    return result == ConnectivityResult.mobile || result == ConnectivityResult.wifi;
  }

  void dispose() {
    _controller.close();
  }
}
