import 'package:flutter_demo/data_providers/products_provider.dart';
import 'package:flutter_demo/models/dtos/product_dto.dart';


class ProductsRepository {
  final ProductsProvider dataProvider = ProductsProvider();

  ProductsRepository();

  Future<List<ProductDto>> fetchData() async {
    return await dataProvider.fetchMockData();
  }

  Future<void> removeItem(String id) async {
    await dataProvider.removeItem(id);
  }

  Future<void> addItem(ProductDto item) async {
    await dataProvider.addItem(item);
  }

  Future<void> synkData(List<ProductDto> products) async {
    await dataProvider.synkData(products);
  }
}
