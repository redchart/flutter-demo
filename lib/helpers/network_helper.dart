
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_demo/blocs/internet/internet_bloc.dart';
import 'package:flutter_demo/blocs/internet/internet_event.dart';

class InternetHelper {

  static void observeNetwork() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        InternetBloc().add(InternetNotify());
      } else {
        InternetBloc().add(InternetNotify(isConnected: true));
      }
    } as void Function(ConnectivityResult event)?);
  }
}