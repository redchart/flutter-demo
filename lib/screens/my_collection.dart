import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/blocs/products/products_bloc.dart';
import 'package:flutter_demo/blocs/products/products_event.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/repositories/data_repository.dart';
import 'package:flutter_demo/typography/headline.dart';
import 'package:flutter_demo/widgets/products_list.dart';

class MyCollection extends StatefulWidget {
  const MyCollection({super.key});

  @override
  State<MyCollection> createState() => _MyCollectionState();
}

class _MyCollectionState extends State<MyCollection> {
  late final ProductsRepository _productsRepository;

  @override
  void initState() {
    super.initState();
    _productsRepository = ProductsRepository();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainBgColor,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80.0), // Adjust the height as needed
        child: Container(
          padding: const EdgeInsets.only(top: 24.0), // Adjust the padding as needed
          child: AppBar(
            surfaceTintColor: mainBgColor,
            title: Headline(
              text: 'My Collection',
              color: white,
            ),
            automaticallyImplyLeading: false,
            actions: [
              IconButton(
                icon: const Icon(Icons.notification_add),
                onPressed: () {},
              ),
            ],
            backgroundColor: mainBgColor,
          ),
        ),
      ),
      body: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: ProductsList(),
      ),
    );
  }
}
