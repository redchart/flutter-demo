import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/typography/body_large.dart';
import 'package:flutter_demo/typography/button_large.dart';
import 'package:flutter_demo/typography/headline_large.dart';
import 'package:flutter_demo/widgets/password_input.dart';
import 'package:flutter_demo/widgets/primary_button.dart';
import 'package:flutter_demo/widgets/text_input.dart';
import 'package:go_router/go_router.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: secondaryBgColor,
      ),
      backgroundColor: secondaryBgColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HeadlineLarge(
                color: white,
                text: 'Sign In',
              ),
              const SizedBox(height: 20.0),
              const CustomTextField(
                labelText: 'Email',
                hintText: 'email@email.com',
              ),
              const SizedBox(height: 20.0),
              const PasswordField(),
              Row(
                children: [
                  Expanded(
                    child: PrimaryButton(
                      onPressed: () {
                        context.go('/my-collection');
                      },
                      bgColor: yellowColor,
                      textColor: black,
                      text: "Continue",
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //add text with white color
                  BodyLarge(
                    text: "Can't sign in?",
                    color: disabledWhite,
                  ),
                  const SizedBox(width: 24.0),
                  TextButton(
                    onPressed: () {
                      context.go('/my-collection');
                    },
                    child: ButtonLarge(
                      text: 'Recover password',
                      color: yellowColor,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
