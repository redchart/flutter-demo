import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/blocs/products/products_bloc.dart';
import 'package:flutter_demo/blocs/products/products_event.dart';
import 'package:flutter_demo/blocs/products/products_state.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/models/dtos/product_dto.dart';
import 'package:flutter_demo/typography/body_small.dart';
import 'package:flutter_demo/typography/headline_large.dart';
import 'package:flutter_demo/widgets/details_tab_bar.dart';
import 'package:flutter_demo/widgets/primary_button.dart';
import 'package:flutter_demo/widgets/product_details.dart';
import 'package:go_router/go_router.dart';

class ProductDetailsScreen extends StatefulWidget {
  const ProductDetailsScreen({
    super.key,
    required this.id,
  });

  final String id;

  @override
  State<ProductDetailsScreen> createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainBgColor,
        elevation: 0,
        surfaceTintColor: mainBgColor,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: white,
          ),
          onPressed: () {
            context.pop();
          },
        ),
      ),
      extendBodyBehindAppBar: true,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: ProductDetailsView(id: widget.id),
      ),
    );
  }
}

class ProductDetailsView extends StatefulWidget {
  const ProductDetailsView({
    super.key,
    required this.id,
  });
  final String id;
  @override
  State<ProductDetailsView> createState() => _ProductDetailsViewState();
}

class _ProductDetailsViewState extends State<ProductDetailsView>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bg_image.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        BlocBuilder<ProductsBloc, ProductsState>(
          builder: (context, state) {
            if (state.status == ProductsStatus.initial) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (state.status == ProductsStatus.failure) {
              return Center(
                child: Text(
                  'Error: ${state.error}',
                ),
              );
            } else if (state.status == ProductsStatus.success) {
              ProductDto? product = state.products.firstWhereOrNull(
                (product) => product.id == widget.id,
              );

              if (product == null) {
                return const Center(child: Text('Product not found'));
              }
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(
                        height: kToolbarHeight + 20), // Space for AppBar
                    Text(
                      'Genuine Bottle (Unopened)',
                      style: TextStyle(color: white, fontSize: 18),
                    ),
                    const SizedBox(height: 10),
                    product.imageUrl.isNotEmpty
                        ? Image.network(
                            product.imageUrl, // Your bottle image
                            height: 300,
                          )
                        : Image.asset(
                            'assets/images/product.png', // Your bottle image
                            height: 300,
                          ),
                    const SizedBox(height: 20),
                    _renderProductDetail(product),
                    const SizedBox(height: 20),
                    _renderRemoveButton(product.id),
                  ],
                ),
              );
            }
            return Container(); // Return an empty container if no valid state
          },
        ),
      ],
    );
  }

  Widget _renderRemoveButton(String id) {
    return PrimaryButton(
      bgColor: yellowColor,
      textColor: black,
      text: "Remove from my collection",
      onPressed: () {
        context.read<ProductsBloc>().add(
              ProductsRemoved(id),
            );
        context.go('/my-collection');
      },
      icon: Icon(
        Icons.remove,
        color: black,
      ),
    );
  }

  Widget _renderProductDetail(ProductDto product) {
    return Container(
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: secondaryBgColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BodySmall(
            text: product.description,
            color: disabledWhite,
          ),
          const SizedBox(height: 10),
          Wrap(
            spacing: 8.0,
            children: [
              HeadlineLarge(
                color: white,
                text: product.title,
              ),
              HeadlineLarge(
                color: yellowColor,
                text: product.details.ageStatement,
              ),
            ],
          ),
          HeadlineLarge(color: white, text: product.details.caskNumber),
          const SizedBox(height: 20),
          DetailsTapBar(
            tabController: _tabController,
          ),
          const SizedBox(height: 10),
          ProductDetailsWidget(details: product.details),
        ],
      ),
    );
  }
}
