import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/typography/body_large.dart';
import 'package:flutter_demo/typography/button_large.dart';
import 'package:flutter_demo/typography/headline_large.dart';
import 'package:flutter_demo/widgets/bg_container.dart';
import 'package:flutter_demo/widgets/primary_button.dart';
import 'package:go_router/go_router.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          // Background Image
          const BgContainer(),
          // Welcome Content
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
              padding: const EdgeInsets.symmetric(vertical: 24),
              decoration: BoxDecoration(
                color: secondaryBgColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  HeadlineLarge(
                    text: "Welcome!",
                    color: white,
                  ),
                  const SizedBox(height: 16.0),
                  BodyLarge(
                    text: 'text text text',
                    color: white,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: PrimaryButton(
                            bgColor: yellowColor,
                            textColor: black,
                            text: 'Scan bottle',
                            onPressed: () {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      BodyLarge(
                        text: 'Have an account?',
                        color: disabledWhite,
                      ),
                      const SizedBox(width: 24.0),
                      TextButton(
                        onPressed: () {
                          context.go('/sign-in');
                        },
                        child: ButtonLarge(
                          text: 'Sign in',
                          color: yellowColor,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
