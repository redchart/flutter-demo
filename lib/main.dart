import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/blocs/internet/internet_bloc.dart';
import 'package:flutter_demo/blocs/internet/internet_event.dart';
import 'package:flutter_demo/blocs/products/products_bloc.dart';
import 'package:flutter_demo/blocs/products/products_event.dart';
import 'package:flutter_demo/config/router.dart';
import 'package:flutter_demo/repositories/connectivity_repository.dart';
import 'package:flutter_demo/repositories/data_repository.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              ProductsBloc(ProductsRepository(),ConnectivityRepository() )..add(ProductsLoad()),
              
        ),
        // BlocProvider(
        //   create:  (context) => InternetBloc()..add(InternetObserve()),
              
        // ),
      ],
      child: MaterialApp.router(
        title: 'Flutter Demo',
        theme: _buildTheme(Brightness.dark),
        routerConfig: router,
      ),
    );
  }

  ThemeData _buildTheme(brightness) {
    var baseTheme = ThemeData(brightness: brightness);

    return baseTheme.copyWith(
      textTheme: GoogleFonts.latoTextTheme(baseTheme.textTheme),
    );
  }
}
