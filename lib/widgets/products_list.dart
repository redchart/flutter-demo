import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/blocs/products/products_bloc.dart';
import 'package:flutter_demo/blocs/products/products_event.dart';
import 'package:flutter_demo/blocs/products/products_state.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/typography/body_small.dart';
import 'package:flutter_demo/typography/title_large.dart';
import 'package:go_router/go_router.dart';

class ProductsList extends StatefulWidget {
  const ProductsList({super.key});

  @override
  State<ProductsList> createState() => _ProductsListState();
}

class _ProductsListState extends State<ProductsList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsBloc, ProductsState>(
      builder: (context, state) {
        switch (state.status) {
          case ProductsStatus.failure:
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(state.error),
                ElevatedButton(
                  onPressed: () {
                    context.read<ProductsBloc>().add(ProductsLoad());
                  },
                  child: const Text('Retry'),
                ),
              ],
            ));
          case ProductsStatus.success:
            if (state.products.isEmpty) {
              return const Center(
                  child: Text('No products in your collection'));
            }
            return LayoutBuilder(
              builder: (context, constraints) {
                final itemHeight = constraints.maxHeight / 2;
                final itemWidth = constraints.maxWidth / 2;

                return GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 8.0,
                    mainAxisSpacing: 9.0,
                    childAspectRatio: itemWidth / itemHeight,
                  ),
                  itemCount: state.products.length,
                  itemBuilder: (context, index) {
                    final product = state.products[index];
                    return GestureDetector(
                      onTap: () {
                        context.push('/products/${product.id}');
                      },
                      child: Card(
                        shape: const BeveledRectangleBorder(),
                        color: secondaryBgColor,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.network(
                                    product.imageUrl,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0, vertical: 4.0),
                              child: TitleLarge(
                                text: product.title,
                                color: white,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0, vertical: 16.0),
                              child: BodySmall(
                                text: product.description,
                                color: Color(0xFFD7D5D1),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            );
          case ProductsStatus.initial:
            return const Center(
              child: CircularProgressIndicator(),
            );
        }
      },
    );
  }
}
