import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';

class CustomTextField extends StatelessWidget {
  final String labelText;
  final String hintText;
  final bool obscureText;
  final Widget? suffixIcon;

  const CustomTextField({
    super.key,
    required this.labelText,
    required this.hintText,
    this.obscureText = false,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: obscureText,
      onTapOutside: (event) {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(color: yellowColor), // Yellow color
        hintText: hintText,
        hintStyle: TextStyle(color: white),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: yellowColor), // Yellow underline
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: yellowColor), // Yellow underline
        ),
        suffixIcon: suffixIcon,
      ),
      style: TextStyle(color: white), // White text color
    );
  }
}
