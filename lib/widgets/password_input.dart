import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/widgets/text_input.dart';

class PasswordField extends StatefulWidget {
  const PasswordField({super.key});

  @override
  State<PasswordField> createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool _obscureText = true;

  void _togglePasswordVisibility() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomTextField(
      labelText: 'Password',
      hintText: 'Password',
      obscureText: _obscureText,
      suffixIcon: IconButton(
        icon: Icon(
          _obscureText ? Icons.visibility_off : Icons.visibility,
          color: yellowColor, // Yellow color
        ),
        onPressed: _togglePasswordVisibility,
      ),
    );
  }
}