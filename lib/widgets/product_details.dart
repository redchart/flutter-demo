import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/models/dtos/product_dto.dart';
import 'package:flutter_demo/typography/body_large.dart';

class ProductDetailsWidget extends StatelessWidget {
  const ProductDetailsWidget({super.key, required this.details});
  final ProductDetails details;

  @override
  Widget build(BuildContext context) {
    return Table(
      children: [
        TableRow(
          children: [
            BodyLarge(text: 'Distillery', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.distillery,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Region', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.region,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Country', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.country,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Type', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.type,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Age statement', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.ageStatement,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Filled', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.filled,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Bottled', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.bottled,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Cask number', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.caskNumber,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'ABV', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.abv,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Size', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.size,
                color: disabledWhite,
              ),
            ),
          ],
        ),
        TableRow(
          children: [
            BodyLarge(text: 'Finish', color: white),
            Container(
              alignment: Alignment.centerRight,
              child: BodyLarge(
                text: details.finish,
                color: disabledWhite,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
