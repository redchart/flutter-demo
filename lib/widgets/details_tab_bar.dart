import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';

class DetailsTapBar extends StatelessWidget {
  const DetailsTapBar({super.key, required this.tabController});
  final TabController tabController;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 32,
      decoration: BoxDecoration(
        color: const Color(0xFF0e1c21), // Background color for unselected tabs
        borderRadius: BorderRadius.circular(6),
      ),
      child: TabBar(
        indicatorColor: Colors.red,
        controller: tabController,
        indicator: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: yellowColor, // Changes the indicator color
        ),
        dividerColor: Colors.transparent,
        labelStyle: TextStyle(
          color: white,
          fontSize: 12.0,
          fontWeight: FontWeight.w400,
          fontFamily: 'lato',
        ),
        indicatorSize: TabBarIndicatorSize.tab,
        labelColor: black,
        unselectedLabelColor: white,
        tabs: const [
          Tab(
            text: 'Details',
          ),
          Tab(
            text: 'Tasting notes',
          ),
          Tab(
            text: 'History',
          ),
        ],
      ),
    );
  }
}
