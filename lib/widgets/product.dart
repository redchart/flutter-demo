import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';
import 'package:flutter_demo/models/dtos/product_dto.dart';

class ProductWidget extends StatelessWidget {
  final ProductDto item;

  const ProductWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: black, // Set card background color to black
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Image.asset(
              item.imageUrl,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              item.title,
              style: TextStyle(
                color: white, // Set title text color to white
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              item.description,
              style: TextStyle(
                color: white, // Set description text color to white
                fontSize: 14,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
