import 'package:flutter/material.dart';
import 'package:flutter_demo/typography/button_large.dart';

class PrimaryButton extends StatelessWidget {
  const PrimaryButton(
      {super.key,
      required this.bgColor,
      required this.textColor,
      required this.text,
      required this.onPressed,
      this.icon});

  final Color bgColor;
  final Color textColor;
  final String text;
  final Function() onPressed;
  final Icon? icon;

  @override
  Widget build(BuildContext context) {
    Widget content;

    if (icon != null) {
      content = Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon!,
          const SizedBox(width: 8.0),
          ButtonLarge(
            text: text,
            color: textColor,
          ),
        ],
      );
    } else {
      content = ButtonLarge(
        text: text,
        color: textColor,
      );
    }
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 24.0,
        // horizontal: 24.0,
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          backgroundColor: bgColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 18.0),
        ),
        child: content,
      ),
    );
  }
}
