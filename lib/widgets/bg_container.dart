import 'package:flutter/material.dart';
import 'package:flutter_demo/config/colors.dart';

class BgContainer extends StatelessWidget {
  const BgContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: const AssetImage('assets/images/bg_image.png'),
          colorFilter: ColorFilter.mode(
            // Colors.blue.withOpacity(0.6),
            mainBgColor,
            BlendMode.lighten,
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
