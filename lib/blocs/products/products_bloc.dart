import 'dart:async';

import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/blocs/products/products_event.dart';
import 'package:flutter_demo/blocs/products/products_state.dart';
import 'package:flutter_demo/models/dtos/product_dto.dart';
import 'package:flutter_demo/repositories/connectivity_repository.dart';
import 'package:flutter_demo/repositories/data_repository.dart';
import 'package:stream_transform/stream_transform.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductsRepository repository;
  final ConnectivityRepository connectivityRepository;
  late final StreamSubscription connectivitySubscription;
  late bool needToSync = false;
  late bool isConnected;

  ProductsBloc(
    this.repository,
    this.connectivityRepository,
  ) : super(const ProductsState()) {
    on<ProductsLoad>(
      _onProductsLoad,
      transformer: throttleDroppable(throttleDuration),
    );
    on<ProductsRemoved>(
      _onProductsRemoved,
    );
    connectivityRepository.isConnected().then((value){
      isConnected = value;
    });
    
    connectivitySubscription =
        connectivityRepository.connectivityStream.listen((connected) async {

      if (isConnected == false && connected == true) {
        isConnected = connected;
        if (needToSync) {
          await repository.synkData(state.products);
          needToSync = false;
        }
      }
      isConnected = connected;
    });
  }

  Future<bool> _isConnected() async {
    bool isConnected = await connectivityRepository.isConnected();
    return isConnected;
  }

  @override
  Future<void> close() {
    // connectivitySubscription.cancel();
    return super.close();
  }

  void _onProductsRemoved(
      ProductsRemoved event, Emitter<ProductsState> emit) async {
    try {
      final List<ProductDto> products;
      bool isConnected = await _isConnected();
      if (isConnected == false) {
        needToSync = true;
        products =
            state.products.where((element) => element.id != event.id).toList();
      } else {
        await repository.removeItem(event.id);
        products = await repository.fetchData();
      }

      emit(state.copyWith(status: ProductsStatus.success, products: products));
    } catch (e) {
      emit(state.copyWith(status: ProductsStatus.failure, error: e.toString()));
    }
  }

  void _onProductsLoad(ProductsLoad event, Emitter<ProductsState> emit) async {
    try {
      bool isConnected = await _isConnected();
      if (isConnected == false) {
        return emit(state.copyWith(
            status: ProductsStatus.failure,
            error:
                "No internet connection, please connect to the internet and try again."));
      }
      final products = await repository.fetchData();
      emit(state.copyWith(status: ProductsStatus.success, products: products));
    } catch (e) {
      emit(state.copyWith(status: ProductsStatus.failure, error: e.toString()));
    }
  }
}
