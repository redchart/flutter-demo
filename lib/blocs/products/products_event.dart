import 'package:equatable/equatable.dart';
import 'package:flutter_demo/models/dtos/product_dto.dart';

sealed class ProductsEvent  extends Equatable {
  @override
  List<Object> get props => [];
}

class ProductsLoad extends ProductsEvent {}

class ProductsRefresh extends ProductsEvent {}

class ProductsRemoved extends ProductsEvent {
  final String id;

  ProductsRemoved(this.id);
}

class ProductAdd extends ProductsEvent {
  final ProductDto product;

  ProductAdd(this.product);
}

