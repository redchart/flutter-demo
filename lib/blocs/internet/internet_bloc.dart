import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_demo/blocs/internet/internet_event.dart';
import 'package:flutter_demo/blocs/internet/internet_state.dart';
import 'package:flutter_demo/helpers/network_helper.dart';

class InternetBloc extends Bloc<InternetEvent, InternetState> {
  InternetBloc._() : super(InternetInitial()) {
    on<InternetObserve>(_observe);
    on<InternetNotify>(_notifyStatus);
  }

  static final InternetBloc _instance = InternetBloc._();

  factory InternetBloc() => _instance;

  void _observe(event, emit) {
    InternetHelper.observeNetwork();
  }

  void _notifyStatus(InternetNotify event, emit) {
    event.isConnected ? emit(InternetSuccess()) : emit(InternetFailure());
  }
}