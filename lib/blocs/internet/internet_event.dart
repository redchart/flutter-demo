sealed class InternetEvent {}

class InternetObserve extends InternetEvent {}

class InternetNotify extends InternetEvent {
  final bool isConnected;

  InternetNotify({this.isConnected = false});
}
