import 'package:flutter/material.dart';

class BodySmall extends StatelessWidget {
  const BodySmall({
    super.key,
    required this.color,
    required this.text,
  });

  final Color color;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: 12.0,
        fontWeight: FontWeight.w400,
        fontFamily: 'lato',
      ),
    );
  }
}
