import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_demo/models/dtos/product_dto.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductsProvider {
  static const _keyProducts = 'products';

  Future<void> _loadMockDataIfNeeded() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(_keyProducts)) {
      final response = await rootBundle.loadString('assets/data/mockup_data.json');
      List<dynamic> data = json.decode(response);
      prefs.setString(_keyProducts, json.encode(data));
    }
  }

  Future<List<ProductDto>> fetchMockData() async {
    await _loadMockDataIfNeeded();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = prefs.getString(_keyProducts);
    List<dynamic> data = json.decode(response!);
    return data.map((json) => ProductDto.fromJson(json)).toList();
  }

  Future<void> removeItem(String id) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = prefs.getString(_keyProducts);
    List<dynamic> data = json.decode(response!);
    data.removeWhere((item) => item['id'] == id);
    prefs.setString(_keyProducts, json.encode(data));
  }

  Future<void> addItem(ProductDto item) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = prefs.getString(_keyProducts);
    List<dynamic> data = json.decode(response!);
    data.add(item.toJson());
    prefs.setString(_keyProducts, json.encode(data));
  }

  Future<void> synkData(List<ProductDto> products) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_keyProducts, json.encode(products));
  }
}
