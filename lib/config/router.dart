import 'package:flutter/material.dart';
import 'package:flutter_demo/screens/my_collection.dart';
import 'package:flutter_demo/screens/products_details_screen.dart';
import 'package:flutter_demo/screens/sign_in_screen.dart';
import 'package:flutter_demo/screens/welcome_screen.dart';
import 'package:go_router/go_router.dart';

final GoRouter router = GoRouter(
  initialLocation: '/',
  routes: <RouteBase>[
    GoRoute(
      path: '/',
      builder: (BuildContext context, GoRouterState state) {
        return const WelcomeScreen();
      },
      routes: <RouteBase>[
        GoRoute(
          path: 'sign-in',
          builder: (BuildContext context, GoRouterState state) {
            return const SignInScreen();
          },
        ),
        GoRoute(
          path: 'my-collection',
          builder: (BuildContext context, GoRouterState state) {
            return const MyCollection();
          },
        ),
        GoRoute(
          path: 'products/:id',
          builder: (BuildContext context, GoRouterState state) {
            String id = state.pathParameters['id']!;
            return ProductDetailsScreen(id: id);
          },
        ),
      ],
    ),
  ],
);
