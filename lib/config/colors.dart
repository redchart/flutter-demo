import 'dart:ui';

Color mainBgColor = const Color(0xFF0b1519);
Color secondaryBgColor = const Color(0xFF12232a);
Color yellowColor = const Color(0xFFD49A00);
Color disabledWhite = const Color(0xFFB8BDBF);
Color white = const Color(0xFFFFFFFF);
Color black = const Color(0xFF000000);
