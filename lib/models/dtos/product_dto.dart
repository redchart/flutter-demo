class ProductDto {
  final String id;
  final String imageUrl;
  final String title;
  final String description;
  final ProductDetails details;

  ProductDto({
    required this.id,
    required this.imageUrl,
    required this.title,
    required this.description,
    required this.details,
  });

  factory ProductDto.fromJson(Map<String, dynamic> json) {
    return ProductDto(
      id: json['id'],
      imageUrl: json['imageUrl'] ?? '',
      title: json['title'] ?? 'No title',
      description: json['description'] ?? 'No description',
      details: ProductDetails.fromJson(json['details'] ?? {}),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'imageUrl': imageUrl,
      'title': title,
      'description': description,
    };
  }
}

class ProductDetails {
  final String distillery;
  final String region;
  final String country;
  final String type;
  final String ageStatement;
  final String filled;
  final String bottled;
  final String caskNumber;
  final String abv;
  final String size;
  final String finish;

  ProductDetails({
    required this.distillery,
    required this.region,
    required this.country,
    required this.type,
    required this.ageStatement,
    required this.filled,
    required this.bottled,
    required this.caskNumber,
    required this.abv,
    required this.size,
    required this.finish,
  });

  factory ProductDetails.fromJson(Map<String, dynamic> json) {
    return ProductDetails(
      distillery: json['distillery'] ?? 'No distillery',
      region: json['region'] ?? 'No region',
      country: json['country'] ?? 'No country',
      type: json['type'] ?? 'No type',
      ageStatement: json['ageStatement'] ?? 'No age statement',
      filled: json['filled'] ?? 'No filled',
      bottled: json['bottled'] ?? 'No bottled',
      caskNumber: json['caskNumber'] ?? 'No cask number',
      abv: json['abv'] ?? 'No abv',
      size: json['size'] ?? 'No size',
      finish: json['finish'] ?? 'No finish',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'distillery': distillery,
      'region': region,
      'country': country,
      'type': type,
      'ageStatement': ageStatement,
      'filled': filled,
      'bottled': bottled,
      'caskNumber': caskNumber,
      'abv': abv,
      'size': size,
      'finish': finish,
    };
  }
}
